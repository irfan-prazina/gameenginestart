class Igrac extends Slusaoc{
    constructor(tekstura,pozicija){
        super("GlavniIgrac");
        this.tekstura=tekstura;
        this.pozicija=pozicija;
        Sabirnica.dodajSlusaoca(this);
        this.slika = new Image;
        this.slika.onload=function(){
            Sabirnica.obavijesti(new Poruka("dodanObjekat",{ime:"GlavniIgrac",img:this,pozicija:{x:pozicija.x,y:pozicija.y}}));
        }
        this.slika.src=tekstura;
    }
    primi(poruka){
        switch(poruka.tip){
            case "pomjeriIgraca":
                this.pozicija.x+=poruka.podaci.dx;
                this.pozicija.y+=poruka.podaci.dy;
                Sabirnica.obavijesti(new Poruka("pomjerenIgrac",{ime:"GlavniIgrac",img:this.slika,pozicija:{x:this.pozicija.x,y:this.pozicija.y}}))
                break;
        }
    }
}