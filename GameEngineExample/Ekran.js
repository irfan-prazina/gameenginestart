class Ekran extends Slusaoc{

    constructor(canvas){
        super("GlavniEkran")
        this.canvas=canvas;
        this.objekti=[];
        Sabirnica.dodajSlusaoca(this);
    }

    iscrtajSve(){
        let ctx = this.canvas.getContext("2d");
        ctx.clearRect(0,0,this.canvas.width,this.canvas.height);
        for(let objekat of this.objekti){
            ctx.drawImage(objekat.img,objekat.pozicija.x,objekat.pozicija.y);
        }
    }
    primi(poruka){
        switch(poruka.tip){
            case "pomjerenIgrac":
                for(let i =0;i<this.objekti.length;i++)if(this.objekti[i].ime==="GlavniIgrac") this.objekti.splice(i,1);
                this.objekti.push(poruka.podaci);
                this.iscrtajSve();
                break;
            case "dodanObjekat":
                this.objekti.push(poruka.podaci);
                this.iscrtajSve();
                break;
        }
    }

}