class Tastatura{
    constructor(document){
        document.addEventListener('keydown',(dugme)=>{
            switch(dugme.code){
                case "ArrowUp":
                    Sabirnica.obavijesti(new Poruka("pomjeriIgraca",{dx:0,dy:-5}));
                    break;
                case "ArrowRight":
                    Sabirnica.obavijesti(new Poruka("pomjeriIgraca",{dx:5,dy:0}));
                    break;    
                case "ArrowLeft":
                    Sabirnica.obavijesti(new Poruka("pomjeriIgraca",{dx:-5,dy:0}));
                    break;    
                case "ArrowDown":
                    Sabirnica.obavijesti(new Poruka("pomjeriIgraca",{dx:0,dy:5}));
                    break;
            }
        });
    }
}