let Sabirnica = (function(){
    let slusaoci=[];
    let dodajSlusaoca = function(objekat){
        slusaoci.push(objekat);
    }
    let obrisiSlusaoca = function(objekat){
        slusaoci = slusaoci.filter(e => e.ime !== objekat.ime);
    }

    let obavijesti = function(novaPoruka){
        console.log(JSON.stringify(novaPoruka));
        for(let slusaoc of slusaoci){
            slusaoc.primi(novaPoruka);
        }
    }
    return {dodajSlusaoca:dodajSlusaoca,obrisiSlusaoca:obrisiSlusaoca,obavijesti:obavijesti};
}());